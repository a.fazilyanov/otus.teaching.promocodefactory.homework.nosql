﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;

namespace Otus.Teaching.Pcf.References.WebHost.Cache
{
    public interface ICacheProvider<T>
    {
        Task<List<T>> GetFromCache();

        Task SetCache(List<T> value);

        Task SetCache(List<T> value, DistributedCacheEntryOptions options);

        Task ClearCache();
    }
}
