﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.References.WebHost.Cache
{
    public class CacheProvider<T> : ICacheProvider<T>
    {
        private readonly IDistributedCache _cache;

        private string key;
        public string  Key {
            get => string.IsNullOrWhiteSpace(key) ? typeof(T).Name : key;
            set => key = value;
        }

        public CacheProvider(IDistributedCache cache)
        {
            _cache = cache;
        }

        public async Task<List<T>> GetFromCache() 
        {
            var cachedResponse = await _cache.GetStringAsync(Key);
            return cachedResponse == null
                ? new List<T>()
                : JsonConvert.DeserializeObject<List<T>>(cachedResponse);
        }

        public async Task SetCache(List<T> value)
        {
            var response = JsonConvert.SerializeObject(value);
            await _cache.SetStringAsync(Key, response);
        }

        public async Task SetCache(List<T> value, DistributedCacheEntryOptions options)
        {
            var response = JsonConvert.SerializeObject(value);
            await _cache.SetStringAsync(Key, response, options);
        }

        public async Task ClearCache()
        {
            await _cache.RemoveAsync(Key);
        }
    }
}
