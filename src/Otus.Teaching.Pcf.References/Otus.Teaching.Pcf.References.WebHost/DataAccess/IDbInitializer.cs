﻿namespace Otus.Teaching.Pcf.References.WebHost.DataAccess
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}
