﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.References.WebHost.Models;

namespace Otus.Teaching.Pcf.References.WebHost.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Preference> Preferences { get; set; }

        public DataContext()
        {

        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }
    }
}
