﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.References.WebHost.Cache;
using Otus.Teaching.Pcf.References.WebHost.DataAccess;
using Otus.Teaching.Pcf.References.WebHost.Models;

namespace Otus.Teaching.Pcf.References.WebHost.Controllers
{
    [Route("api/[controller]")]
    public class PreferenceController : ControllerBase
    {
        private readonly ICacheProvider<Preference> _cacheProvider;
        private readonly DataContext _dataContext;
        public PreferenceController(DataContext dataContext, ICacheProvider<Preference> cacheProvider)
        {
            _dataContext = dataContext;
            _cacheProvider = cacheProvider;
        }

        [HttpGet]
        public async Task<IActionResult> GetPreferencesAsync()
        {
            var preferences = await _cacheProvider.GetFromCache();
            if (!preferences.Any())
            {
                preferences = await _dataContext.Preferences.ToListAsync();
                await _cacheProvider.SetCache(preferences);
            }
            return Ok(preferences);
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetPreferenceAsync(Guid id)
        {

            var preferences = await _cacheProvider.GetFromCache();
            if (!preferences.Any())
            {
                preferences = await _dataContext.Preferences.ToListAsync();
                await _cacheProvider.SetCache(preferences);
            }

            var preference = preferences.FirstOrDefault(x => x.Id == id);

            return Ok(preference);
        }
        
    }
}
