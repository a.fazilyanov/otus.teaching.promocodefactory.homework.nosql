﻿using System;

namespace Otus.Teaching.Pcf.References.WebHost.Models
{
    public class Preference
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
