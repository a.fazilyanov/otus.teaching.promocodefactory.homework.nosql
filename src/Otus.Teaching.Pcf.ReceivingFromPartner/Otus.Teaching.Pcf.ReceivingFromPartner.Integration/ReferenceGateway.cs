﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class ReferenceGateway
        : IReferenceGateway
    {
        private readonly HttpClient _httpClient;

        public ReferenceGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<List<Preference>> GetPreferences()
        {
            var response = await _httpClient.GetStringAsync("api/preference");
            return JsonConvert.DeserializeObject<List<Preference>>(response);
        }

        public async Task<Preference> GetPreference(Guid id)
        {
            var response = await _httpClient.GetStringAsync($"api/preference/{id}");
            return JsonConvert.DeserializeObject<Preference>(response);
        }
        
    }
}