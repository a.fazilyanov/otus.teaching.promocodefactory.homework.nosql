﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways
{
    public interface IReferenceGateway
    {
        Task<List<Preference>> GetPreferences();

        Task<Preference> GetPreference(Guid id);
    }
}